<?php
declare(strict_types=1);

namespace DesignPatterns\Structural\Flyweight;

use App\DesignPatterns\Structural\Flyweight\CharacterFactory;
use App\DesignPatterns\Structural\Flyweight\Document\DocumentWithFlyweight;
use App\DesignPatterns\Structural\Flyweight\Document\DocumentWithoutFlyweight;
use PHPUnit\Framework\TestCase;

class DocumentTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testDocument(): void
    {
        $documentWithFlyweight = new DocumentWithFlyweight('Ala ma kota a kot ma AIDS', new CharacterFactory());
        $documentWithoutFlyweight = new DocumentWithoutFlyweight('Ala ma kota a kot ma AIDS');

        echo $documentWithFlyweight->toString() . PHP_EOL;
        echo 'With flyweight object number: ' . $documentWithFlyweight->countValues() . PHP_EOL;
        echo 'Without flyweight object number: ' . $documentWithoutFlyweight->countValues() . PHP_EOL;
    }
}
