<?php
declare(strict_types=1);

namespace DesignPatterns\Structural\Adapter;

use App\DesignPatterns\Structural\Adapter\Authentication\GoogleUserAuthentication;
use App\DesignPatterns\Structural\Adapter\Authentication\GoogleUserAuthenticationAdapter;
use App\DesignPatterns\Structural\Adapter\Authentication\UserAuthentication;
use App\DesignPatterns\Structural\Adapter\FileManager;
use PHPUnit\Framework\TestCase;

class FileManagerTest extends TestCase
{
    public function testFileManager(): void
    {
        $userAuthentication = new UserAuthentication();
        $userAuthentication->authenticate('user', 'pass');

        $googleUserAuthentication = new GoogleUserAuthenticationAdapter(new GoogleUserAuthentication());
        $googleUserAuthentication->authenticate('user', 'pass');

        echo 'Recent content with user authentication:' . PHP_EOL;
        $fileManager = new FileManager($userAuthentication);
        self::assertEquals('File content.', $fileManager->getRecentFileContent());

        echo PHP_EOL;
        echo 'Recent content with google user authentication:' . PHP_EOL;
        $fileManager = new FileManager($googleUserAuthentication);
        self::assertEquals('File content.', $fileManager->getRecentFileContent());
    }
}
