<?php
declare(strict_types=1);

namespace DesignPatterns\Structural\Decorator;

use App\DesignPatterns\Structural\Decorator\Stream\FileStream;
use App\DesignPatterns\Structural\Decorator\Stream\StreamMode\AsciiStream;
use App\DesignPatterns\Structural\Decorator\Stream\StreamMode\CompressStream;
use PHPUnit\Framework\TestCase;

class StreamTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testStreams(): void
    {
        $stream = new CompressStream(new FileStream('data'));
        $stream->add('Koziołek matołek tralalala hopsa hopsa sa');
        $stream->save();

        $data = file_get_contents('data');
        echo 'Uncompressed -> Size: ' . strlen(gzuncompress($data)) . ' Value: ' . gzuncompress($data) . PHP_EOL;
        echo 'Compressed -> Size: ' . strlen($data) . ' Value: ' . $data . PHP_EOL;

        $stream = new AsciiStream(new FileStream('data'));
        $stream->add('Kamilka tralalala hopsa hopsa sa');
        $stream->save();

        $data = file_get_contents('data');
        echo 'Ascii: ' . $data . PHP_EOL;

        echo PHP_EOL;

        $stream = new AsciiStream(new CompressStream(new FileStream('data')));
        $stream->add('Kamilka tralalala hopsa hopsa sa');
        $stream->save();

        $data = file_get_contents('data');
        echo 'Uncompressed -> Size: ' . strlen(gzuncompress($data)) . ' Value: ' . gzuncompress($data) . PHP_EOL;
        echo 'Compressed -> Size: ' . strlen($data) . ' Value: ' . $data . PHP_EOL;
    }
}
