<?php
declare(strict_types=1);

namespace DesignPatterns\Structural\Bridge;

use App\DesignPatterns\Structural\Bridge\Tv\SamsungTv;
use App\DesignPatterns\Structural\Bridge\TvControl\ManualControl;
use App\DesignPatterns\Structural\Bridge\TvControl\RemoteControl;
use PHPUnit\Framework\TestCase;

class TvTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testTV(): void
    {
        $samsungTv = new SamsungTv();

        echo 'Samsung manual control:' . PHP_EOL;
        $samsungTv->setControlSystem(new ManualControl());
        $samsungTv->on();
        $samsungTv->changeChannel(22);
        $samsungTv->off();

        echo PHP_EOL . PHP_EOL;

        echo 'Samsung remote control:' . PHP_EOL;
        $samsungTv->setControlSystem(new RemoteControl());
        $samsungTv->on();
        $samsungTv->changeChannel(13);
        $samsungTv->off();
    }
}
