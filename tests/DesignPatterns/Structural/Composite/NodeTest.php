<?php
declare(strict_types=1);

namespace DesignPatterns\Structural\Composite;

use App\DesignPatterns\Structural\Composite\Node\Number;
use App\DesignPatterns\Structural\Composite\Node\Operation\Addition;
use App\DesignPatterns\Structural\Composite\Node\Operation\Subtraction;
use PHPUnit\Framework\TestCase;

class NodeTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testCalculation(): void
    {
        $add1 = new Addition();
        $add1->setLeft(new Number(12));
        $add1->setRgiht(new Number(5));

        $add2 = new Addition();
        $add2->setLeft(new Number(4));
        $add2->setRgiht(new Number(3));

        $substract = new Subtraction();
        $substract->setLeft($add1);
        $substract->setRgiht($add2);

        echo '(12 + 5) - (4 + 3) = ' . $substract->getValue() . PHP_EOL;
    }
}
