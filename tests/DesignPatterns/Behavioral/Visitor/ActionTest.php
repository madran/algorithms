<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Visitor;

use App\DesignPatterns\Behavioral\Visitor\Action\FileActionAddSuffix;
use App\DesignPatterns\Behavioral\Visitor\File\Directory;
use App\DesignPatterns\Behavioral\Visitor\File\ImageFile;
use PHPUnit\Framework\TestCase;

class ActionTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testActionVisitor(): void
    {
        $directory = new Directory('Pictures');
        $directory->addElement(new ImageFile('img1.png'));
        $directory->addElement(new ImageFile('img2.png'));
        $directory->addElement(new ImageFile('img3.png'));

        foreach ($directory->getElements() as $element) {
            echo $element->getName() . PHP_EOL;
        }

        echo PHP_EOL . 'Dodaj prefiks "_ko" do nazw plików:' . PHP_EOL;
        $fileActionAddPrefix = new FileActionAddSuffix('_ok');
        $directory->action($fileActionAddPrefix);

        foreach ($directory->getElements() as $element) {
            echo $element->getName() . PHP_EOL;
        }
    }
}
