<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Strategy;

use App\DesignPatterns\Behavioral\Strategy\ColorPicker;
use App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy\ColorPickerBlendAdjacentColors;
use App\DesignPatterns\Behavioral\Strategy\Image;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testColorPick(): void
    {
        $image = new Image();
        for ($y = 0; $y < $image->getHeight(); $y++) {
            for ($x = 0; $x < $image->getWidth(); $x++) {
                echo $image->getPixel($x, $y)->toString();
            }
            echo PHP_EOL;
        }

        $colorPicker = new ColorPicker();
        echo PHP_EOL;
        echo $colorPicker->pickColor($image, 2, 2)->toString();
        echo PHP_EOL;

        $colorPicker = new ColorPicker(new ColorPickerBlendAdjacentColors());

        echo $colorPicker->pickColor($image, 0, 0)->toString();
        echo PHP_EOL;
    }
}
