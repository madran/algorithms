<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Observer;

use App\DesignPatterns\Behavioral\Observer\EventListener;
use App\DesignPatterns\Behavioral\Observer\Keyboard;
use PHPUnit\Framework\TestCase;

class KeyboardTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testKeyborad(): void
    {
        $keyboard = new Keyboard();
        $eventListenerA = new EventListener('a');
        $eventListenerB = new EventListener('b');

        $keyboard->addEventListener($eventListenerA);
        $keyboard->addEventListener($eventListenerB);

        $keyboard->pressKey('b');
        $keyboard->pressKey('a');
    }
}
