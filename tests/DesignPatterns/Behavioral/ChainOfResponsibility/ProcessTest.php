<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\ChainOfResponsibility;

use App\DesignPatterns\Behavioral\ChainOfResponsibility\Request;
use App\DesignPatterns\Behavioral\ChainOfResponsibility\ReturnValueProcess\ProcessA;
use App\DesignPatterns\Behavioral\ChainOfResponsibility\ReturnValueProcess\ProcessB;
use App\DesignPatterns\Behavioral\ChainOfResponsibility\ReturnValueProcess\ProcessC;
use PHPUnit\Framework\TestCase;

class ProcessTest extends TestCase
{
    public function testProcess(): void
    {
        $processA = new ProcessA();
        $processB = new ProcessB();
        $processC = new ProcessC();

        $processA->addNextProcess($processB);
        $processB->addNextProcess($processC);

        $request = new Request(10);
        $response = $processA->start($request);

        self::assertEquals(10, $response->getValue());
    }
}
