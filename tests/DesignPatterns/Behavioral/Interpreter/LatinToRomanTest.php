<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Interpreter;

use App\DesignPatterns\Behavioral\Interpreter\Context;
use App\DesignPatterns\Behavioral\Interpreter\LatinToRoman\One;
use App\DesignPatterns\Behavioral\Interpreter\LatinToRoman\Ten;
use PHPUnit\Framework\TestCase;

class LatinToRomanTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testConvert(): void
    {
        $number = 'XLVIII';
        $context = new Context($number);

        $ten = new Ten();
        $ten->convert($context);

        $one = new One();
        $one->convert($context);

        echo $context->getOutput();
    }
}
