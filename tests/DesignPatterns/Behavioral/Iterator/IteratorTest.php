<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Iterator;

use App\DesignPatterns\Behavioral\Iterator\Container\ArrayContainer;
use App\DesignPatterns\Behavioral\Iterator\Container\ListContainer;
use PHPUnit\Framework\TestCase;

class IteratorTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testArrayIterator(): void
    {
        $arrayContainer = new ArrayContainer();
        $arrayContainer->add('value 1');
        $arrayContainer->add('value 2');
        $arrayContainer->add('value 3');
        $arrayContainer->add('value 4');
        $arrayContainer->add('value 5');

        $iterator = $arrayContainer->createIterator();

        while (!$iterator->isDone()) {
            echo $iterator->current() . PHP_EOL ;
            $iterator->next();
        }
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testListIterator(): void
    {
        $listContainer = new ListContainer();
        $listContainer->add('value 1');
        $listContainer->add('value 2');
        $listContainer->add('value 3');
        $listContainer->add('value 4');
        $listContainer->add('value 5');


        $iterator = $listContainer->createIterator();

        while (!$iterator->isDone()) {
            echo $iterator->current() . PHP_EOL ;
            $iterator->next();
        }
    }
}
