<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\State;

use App\DesignPatterns\Behavioral\State\UnitState\UnitStateFortify;
use App\DesignPatterns\Behavioral\State\UnitState\UnitStateSentry;
use App\DesignPatterns\Behavioral\State\Warrior;
use PHPUnit\Framework\TestCase;

class WarriorTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testWarriorStates(): void
    {
        $warrior = new Warrior();

        $fortifyState = new UnitStateFortify();
        $sentryState = new UnitStateSentry();

        echo $warrior->toString();
        $warrior->changeState($fortifyState);
        echo $warrior->toString();
        $warrior->changeState($sentryState);
        echo $warrior->toString();
        $warrior->setDefaultState();
        echo $warrior->toString();
    }
}
