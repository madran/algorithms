<?php
declare(strict_types=1);

namespace DesignPatterns\Behavioral\Command;

use App\DesignPatterns\Behavioral\Command\ArithmeticUnit;
use App\DesignPatterns\Behavioral\Command\Calculator;
use App\DesignPatterns\Behavioral\Command\Operation\Add;
use App\DesignPatterns\Behavioral\Command\Operation\Subtract;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testCalculation(): void
    {
        $operations = [
            new Add(5),
            new Add(7),
            new Subtract(3),
            new Add(6),
        ];

        $calculator = new Calculator(new ArithmeticUnit());

        foreach ($operations as $operation) {
            $calculator->addOperation($operation);
        }

        self::assertEquals(0, $calculator->getResult());
        $calculator->executeNext();
        self::assertEquals(5, $calculator->getResult());
        $calculator->executeNext();
        self::assertEquals(12, $calculator->getResult());
        $calculator->executeNext();
        self::assertEquals(9, $calculator->getResult());
        $calculator->executeNext();
        self::assertEquals(15, $calculator->getResult());
        $calculator->undoOperation();
        self::assertEquals(9, $calculator->getResult());
        $calculator->undoOperation();
        self::assertEquals(12, $calculator->getResult());
        $calculator->undoOperation();
        self::assertEquals(5, $calculator->getResult());
        $calculator->undoOperation();
        self::assertEquals(0, $calculator->getResult());
    }
}
