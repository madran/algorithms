<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\ObjectPool;

use App\DesignPatterns\Creational\ObjectPool\Barracks;
use PHPUnit\Framework\TestCase;

class BarracksTest extends TestCase
{
    public function testWar(): void
    {
        $barracks = new Barracks();

        $warriors = array();
        for ($i = 0; $i < 5; $i++) {
            $warrior = $barracks->getWarrior();
            $warriors[] = $warrior;
        }

        foreach ($warriors as $key => $warrior) {
            $warrior->isDead = ($key % 2 === 0);
            $barracks->quarter($warrior);
        }

        $barracks->clearBarracks();

        $nrOfVeterans = 0;

        for ($i = 0; $i < 5; $i++) {
            $warrior = $barracks->getWarrior();
            if ($warrior->isVeteran === true) {
                $nrOfVeterans++;
            }
        }

        $this->assertEquals(2, $nrOfVeterans);
    }
}
