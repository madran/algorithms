<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Builder;

use App\DesignPatterns\Creational\Builder\WindowApp;
use PHPUnit\Framework\TestCase;

class WindowAppTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testStart(): void
    {
        $app = new WindowApp();

        $app->start();
    }
}
