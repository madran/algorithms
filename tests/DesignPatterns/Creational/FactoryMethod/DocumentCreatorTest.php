<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\FactoryMethod;

use App\DesignPatterns\Creational\FactoryMethod\Address;
use App\DesignPatterns\Creational\FactoryMethod\DocumentCreator;
use App\DesignPatterns\Creational\FactoryMethod\User;
use PHPUnit\Framework\TestCase;

class DocumentCreatorTest extends TestCase
{
    public function testProposalDocument(): void
    {
        $address = new Address();
        $address->setCity('Warszawa');
        $address->setStreetName('Mickiewicza');
        $address->setHouseNumber('21');
        $address->setFlatNumber('44');

        $user = new User();
        $user->setFirstName('Stefan');
        $user->setLastName('Kamyczek');
        $user->setAddress($address);

        $documentCreator = new DocumentCreator();

        $proposalDocument = $documentCreator->createDocument('proposal', ['user' => $user]);

        $expected = <<<EOL
        Stefan Kamyczek
        Mickiewicza 21 44
        Warszawa
        
        
        Wniosek
        Bla bla bla że wniosek.
        
        EOL;

        self::assertEquals($expected, $proposalDocument->toString());
    }
}
