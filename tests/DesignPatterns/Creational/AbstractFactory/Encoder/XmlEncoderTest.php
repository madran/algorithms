<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\AbstractFactory\Encoder;

use App\DesignPatterns\Creational\AbstractFactory\Encoder\XmlEncoder;
use PHPUnit\Framework\TestCase;

class XmlEncoderTest extends TestCase
{
    /**
     * @dataProvider toEncode
     */
    public function testEncode(array $data, string $expected): void
    {
        $xmlEncoder = new XmlEncoder();
        $result = $xmlEncoder->encode($data);

        self::assertEquals($expected, $result);
    }

    public function toEncode(): array
    {
        return [
            [
                [
                    'customer' => [
                        'first-name' => 'John',
                        'last-name' => 'Rambo',
                    ]
                ],
                '<?xml version="1.0"?>' .
                PHP_EOL .
                '<customer><first-name>John</first-name><last-name>Rambo</last-name></customer>' .
                PHP_EOL
            ],
            [
                [
                    'first-name' => 'John',
                    'last-name' => 'Rambo',
                ],
                '<?xml version="1.0"?>' .
                PHP_EOL .
                '<root><first-name>John</first-name><last-name>Rambo</last-name></root>' .
                PHP_EOL
            ],
            [
                [
                    'customers' => [
                        'customer-001' => [
                            'first-name' => 'John',
                            'last-name' => 'Rambo',
                        ],
                        'customer-002' => [
                            'first-name' => 'Jen',
                            'last-name' => 'Supermen',
                        ]
                    ]
                ],
                '<?xml version="1.0"?>' .
                PHP_EOL .
                '<customers><customer-001><first-name>John</first-name><last-name>Rambo</last-name></customer-001><customer-002><first-name>Jen</first-name><last-name>Supermen</last-name></customer-002></customers>' .
                PHP_EOL
            ],
            [
                [
                    'customers' => [
                        'customer-001' => [
                            'first-name' => 'John',
                            'last-name' => 'Rambo',
                            'address' => [
                                'city' => 'London',
                                'street' => 'Main Str.',
                                'house-nr' => '12'
                            ]
                        ],
                        'customer-002' => [
                            'first-name' => 'Jen',
                            'last-name' => 'Supermen',
                        ]
                    ]
                ],
                '<?xml version="1.0"?>' .
                PHP_EOL .
                '<customers><customer-001><first-name>John</first-name><last-name>Rambo</last-name><address><city>London</city><street>Main Str.</street><house-nr>12</house-nr></address></customer-001><customer-002><first-name>Jen</first-name><last-name>Supermen</last-name></customer-002></customers>' .
                PHP_EOL
            ]
        ];
    }
}
