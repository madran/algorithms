<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\AbstractFactory;

use App\DesignPatterns\Creational\AbstractFactory\ConverterFactory\JsonConverterFactory;
use App\DesignPatterns\Creational\AbstractFactory\ConverterFactory\XmlConverterFactory;
use App\DesignPatterns\Creational\AbstractFactory\EncodedNumberCalculator;
use PHPUnit\Framework\TestCase;

class EncodedNumberCalculatorTest extends TestCase
{
    public function testSubtractFromJson(): void
    {
        $json = '{"first": 10, "second": 20, "third": 30}';

        $calculator = new EncodedNumberCalculator();

        $result = $calculator->subtract($json, 3, new JsonConverterFactory());

        self::assertEquals(
            '{"first":7,"second":17,"third":27}',
            $result
        );
    }

    public function testSubtractFromXML(): void
    {
        $xml = '<root><first>10</first><second>20</second><third>30</third></root>';

        $calculator = new EncodedNumberCalculator();

        $result = $calculator->subtract($xml, 3, new XmlConverterFactory());

        self::assertEquals(
            '<?xml version="1.0"?>' . PHP_EOL .
                    '<root><first>7</first><second>17</second><third>27</third></root>' . PHP_EOL,
            $result
        );
    }
}
