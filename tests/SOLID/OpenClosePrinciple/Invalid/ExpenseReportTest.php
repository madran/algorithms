<?php
declare(strict_types=1);

namespace SOLID\OpenClosePrinciple\Invalid;

use App\SOLID\OpenClosePrinciple\Invalid\Expense;
use App\SOLID\OpenClosePrinciple\Invalid\ExpenseReport;
use App\SOLID\OpenClosePrinciple\Invalid\ReportPrinter;
use PHPUnit\Framework\TestCase;

class ExpenseReportTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function test(): void
    {
        $dinnerExpense = new Expense(Expense::TYPE_DINNER, 1000);
        $breakfastExpense = new Expense(Expense::TYPE_BREAKFAST, 2000);
        $carRentalExpense = new Expense(Expense::TYPE_CAR_RENTAL, 4000);

        $expenseReport = new ExpenseReport();
        $expenseReport->addExpense($dinnerExpense);
        $expenseReport->addExpense($breakfastExpense);
        $expenseReport->addExpense($carRentalExpense);

        $expenseReport->printReport(new ReportPrinter());
    }
}
