<?php
declare(strict_types=1);

namespace SOLID\OpenClosePrinciple\Valid;

use App\SOLID\OpenClosePrinciple\Valid\Expense\BreakfastExpense;
use App\SOLID\OpenClosePrinciple\Valid\Expense\CarRentalExpense;
use App\SOLID\OpenClosePrinciple\Valid\Expense\DinnerExpense;
use App\SOLID\OpenClosePrinciple\Valid\ExpenseCalculator;
use App\SOLID\OpenClosePrinciple\Valid\ExpenseReport;
use App\SOLID\OpenClosePrinciple\Valid\ReportPrinter;
use PHPUnit\Framework\TestCase;

class ExpenseReportTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function test(): void
    {
        $dinnerExpense = new DinnerExpense(1000);
        $breakfastExpense = new BreakfastExpense(2000);
        $carRentalExpense = new CarRentalExpense(4000);

        $expenses = [$dinnerExpense, $breakfastExpense, $carRentalExpense];

        $expenseReport = new ExpenseReport(
            new ExpenseCalculator(),
            new ReportPrinter()
        );
        $expenseReport->print($expenses);
    }
}
