<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Interpreter;

class Context
{
    private string $input;
    private int $output;

    public function __construct(string $input)
    {
        $this->input = $input;
        $this->output = 0;
    }

    public function getInput(): string
    {
        return $this->input;
    }

    public function setInput(string $input): void
    {
        $this->input = $input;
    }

    public function getOutput(): int
    {
        return $this->output;
    }

    public function setOutput(int $output): void
    {
        $this->output = $output;
    }

    public function startWith($pattern): bool
    {
        return strpos($this->getInput(), $pattern) === 0;
    }
}