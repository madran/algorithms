<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Interpreter;

abstract class LatinToRoman
{
    public function convert(Context $context): void
    {
        if($context->startWith($this->nine())) {
            $context->setOutput($context->getOutput() + 9 * $this->multiply());
            $context->setInput(substr($context->getInput(), 2));
        } else

            if($context->startWith($this->five())) {
                $context->setOutput($context->getOutput() + 5 * $this->multiply());
                $context->setInput(substr($context->getInput(), 1));
            } else

                if($context->startWith($this->four())) {
                    $context->setOutput($context->getOutput() + 4 * $this->multiply());
                    $context->setInput(substr($context->getInput(), 2));
                }

        if($context->startWith($this->one())) {
            while(strpos($context->getInput(), $this->one()) === 0) {
                $context->setOutput($context->getOutput() + 1 * $this->multiply());
                $context->setInput(substr($context->getInput(), 1));
            }
        }
    }

    abstract public function one(): string;
    abstract public function four(): string;
    abstract public function five(): string;
    abstract public function nine(): string;
    abstract public function multiply(): int;
}