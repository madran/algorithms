<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Interpreter\LatinToRoman;

use App\DesignPatterns\Behavioral\Interpreter\LatinToRoman;

class Ten extends LatinToRoman
{
    public function one(): string
    {
        return 'X';
    }

    public function four(): string
    {
        return 'XL';
    }

    public function five(): string
    {
        return 'L';
    }

    public function nine(): string
    {
        return 'XC';
    }

    public function multiply(): int
    {
        return 10;
    }
}