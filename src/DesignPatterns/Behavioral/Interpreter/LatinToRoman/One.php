<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Interpreter\LatinToRoman;

use App\DesignPatterns\Behavioral\Interpreter\LatinToRoman;

class One extends LatinToRoman
{
    public function one(): string
    {
        return 'I';
    }

    public function four(): string
    {
        return 'IV';
    }

    public function five(): string
    {
        return 'V';
    }

    public function nine(): string
    {
        return 'IX';
    }

    public function multiply(): int
    {
        return 1;
    }
}
