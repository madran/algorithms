<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator\Iterator;

use App\DesignPatterns\Behavioral\Iterator\Iterator;

class ArrayIterator implements Iterator
{
    private array $array;

    private int $counter = 0;

    private int $arraySize;

    public function __construct(array $array)
    {
        $this->array = $array;
        $this->arraySize = count($this->array);
    }

    public function first()
    {
        return $this->array[0];
    }

    public function next(): void
    {
        $this->counter++;
    }

    public function current()
    {
        return $this->array[$this->counter];
    }

    public function isDone(): bool
    {
        return $this->counter >= $this->arraySize;
    }
}