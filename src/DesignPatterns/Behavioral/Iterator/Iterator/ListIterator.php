<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator\Iterator;

use App\DesignPatterns\Behavioral\Iterator\Iterator;
use App\DesignPatterns\Behavioral\Iterator\ListElement;

class ListIterator implements Iterator
{
    private ?ListElement $list;
    private ?ListElement $current;

    public function __construct(?ListElement $list)
    {
        $this->list = $list;
        $this->current = $list;
    }

    public function first()
    {
        return $this->list->getValue();
    }

    public function next(): void
    {
        $this->current = $this->current->getNextElement();
    }

    public function current()
    {
        return $this->current->getValue();
    }

    public function isDone(): bool
    {
        return ($this->current === null);
    }
}