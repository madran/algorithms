<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator;

interface Container
{
    public function add($value): void;

    public function createIterator(): Iterator;
}