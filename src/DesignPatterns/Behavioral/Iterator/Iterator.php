<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator;

interface Iterator
{
    /**
     * @return mixed
     */
    public function first();

    public function next(): void;

    /**
     * @return mixed
     */
    public function current();

    public function isDone(): bool;
}