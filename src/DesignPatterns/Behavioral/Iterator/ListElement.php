<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator;

class ListElement
{
    /** @var mixed */
    private $value;

    private ?ListElement $nextElement = null;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getNextElement(): ?ListElement
    {
        return $this->nextElement;
    }

    public function setNextElement(ListElement $nextElement): void
    {
        $this->nextElement = $nextElement;
    }
}