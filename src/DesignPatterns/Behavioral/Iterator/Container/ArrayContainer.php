<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator\Container;

use App\DesignPatterns\Behavioral\Iterator\Container;
use App\DesignPatterns\Behavioral\Iterator\Iterator\ArrayIterator;
use App\DesignPatterns\Behavioral\Iterator\Iterator;

class ArrayContainer implements Container
{
    private array $array;

    public function add($value): void
    {
        $this->array[] = $value;
    }

    public function createIterator(): Iterator
    {
        return new ArrayIterator($this->array);
    }
}