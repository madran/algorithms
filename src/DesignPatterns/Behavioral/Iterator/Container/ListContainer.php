<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Iterator\Container;

use App\DesignPatterns\Behavioral\Iterator\Container;
use App\DesignPatterns\Behavioral\Iterator\Iterator\ListIterator;
use App\DesignPatterns\Behavioral\Iterator\Iterator;
use App\DesignPatterns\Behavioral\Iterator\ListElement;

class ListContainer implements Container
{
    private ?ListElement $current = null;

    private ?ListElement $list = null;

    public function add($value): void
    {
        if ($this->current === null) {
            $this->list = $this->current = new ListElement($value);
            return;
        }

        $next = new ListElement($value);

        $this->current->setNextElement($next);
        $this->current = $next;
    }

    public function createIterator(): Iterator
    {
        return new ListIterator($this->list);
    }
}