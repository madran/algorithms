<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Observer;

class EventListener
{
    private ?Keyboard $eventSource = null;
    private string $eventKey;

    public function __construct($eventKey)
    {
        $this->eventKey = $eventKey;
    }

    public function addsourceOfEvent($source): void
    {
        $this->eventSource = $source;
    }

    public function support(string $key): bool
    {
        return $this->eventKey === $key;
    }

    public function execute(): void
    {
        echo $this->eventKey . ' key pressed' . PHP_EOL;
    }
}