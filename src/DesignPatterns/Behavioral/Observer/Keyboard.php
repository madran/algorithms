<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Observer;

class Keyboard
{
    private string $pressedKey;

    private array $eventListeners = [];

    public function addEventListener(EventListener $listener): void
    {
        $listener->addsourceOfEvent($this);
        $this->eventListeners[] = $listener;
    }

    public function pressKey($key): void
    {
        $this->pressedKey = $key;
        $this->notifyAllListeners();
    }

    private function notifyAllListeners(): void
    {
        foreach ($this->eventListeners as $listener) {
            if ($listener->support($this->pressedKey)) {
                $listener->execute();
            }
        }
    }
}