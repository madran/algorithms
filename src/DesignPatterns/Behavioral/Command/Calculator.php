<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Command;

class Calculator
{
    private int $result;

    private ArithmeticUnit $arithmeticUnit;

    /** @var Operation[] */
    private array $operationList = [];

    private int $currentOperation = 0;

    public function __construct(ArithmeticUnit $arithmeticUnit)
    {
        $this->arithmeticUnit = $arithmeticUnit;
        $this->result = 0;
    }

    public function addOperation(Operation $operation): void
    {
        $operation->setArithmeticUnit($this->arithmeticUnit);
        $this->operationList[] = $operation;
    }

    public function executeNext(): void
    {
        $this->result = $this->operationList[$this->currentOperation]->execute($this->result);
        $this->currentOperation++;
    }

    public function undoOperation(): void
    {
        $this->result = $this->operationList[$this->currentOperation]->execute($this->result);
        $this->currentOperation--;
    }

    public function getResult(): int
    {
        return $this->result;
    }
}