<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Command\Operation;

use App\DesignPatterns\Behavioral\Command\Operation;

class Add extends Operation
{
    public function execute(int $number): int
    {
        return $this->arithmeticUnit->add($this->number, $number);
    }

    public function undo(int $number): int
    {
        return $this->arithmeticUnit->subtract($this->number, $number);
    }
}