<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Command;

abstract class Operation
{
    protected int $number;

    protected ArithmeticUnit $arithmeticUnit;

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    abstract public function execute(int $number): int;

    abstract public function undo(int $number): int;

    public function setArithmeticUnit(ArithmeticUnit $receiver): void
    {
        $this->arithmeticUnit = $receiver;
    }
}