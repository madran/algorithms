<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Command;

class ArithmeticUnit
{
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }

    public function subtract(int $a, int $b): int
    {
        return $a - $b;
    }

    public function multiply(int $a, int $b): int
    {
        return $a * $b;
    }
}