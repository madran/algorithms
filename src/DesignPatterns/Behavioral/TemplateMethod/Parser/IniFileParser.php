<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\TemplateMethod\Parser;

use App\DesignPatterns\Behavioral\TemplateMethod\Parser;

class IniFileParser extends Parser
{
    protected function loadData(string $source): string
    {
        return $source;
    }

    protected function getParsed(string $data): array
    {
        return ['key1' => 'value1', 'key2' => 'value2'];
    }
}