<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\TemplateMethod;

abstract class Parser
{
    public function __construct(){}

    public function parse($source): array
    {
        $data = $this->loadData($source);
        return $this->getParsed($data);
    }

    abstract protected function loadData(string $source): string;
    abstract protected function getParsed(string $data): array;
}