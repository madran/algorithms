<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy;

use App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy\ColorPickerDefault;

class ColorPicker
{
    private ColorPickerStrategy $colorPickMethod;

    private Color $color;

    public function __construct(ColorPickerStrategy $colorPickMethod = null)
    {
        if ($colorPickMethod === null) {
            $this->colorPickMethod = new ColorPickerDefault();
        } else {
            $this->colorPickMethod = $colorPickMethod;
        }
    }

    public function pickColor($image, $x, $y): Color
    {
        return $this->colorPickMethod->pickColor($image, $x, $y);
    }

    public function getColor(): Color
    {
        return $this->color;
    }
}