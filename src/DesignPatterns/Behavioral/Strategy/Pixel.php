<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy;

class Pixel
{
    private int $row;
    private int $column;
    private Color $color;

    public function __construct(int $row, int $column, Color $color)
    {
        $this->row = $row;
        $this->column = $column;
        $this->color = $color;
    }

    public function getRow(): int
    {
        return $this->row;
    }

    public function getColumn(): int
    {
        return $this->column;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function toString(): string
    {
        return '(X=' . $this->column . '|' . 'Y=' . $this->row . '|' . $this->color->toString() . ')';
    }
}