<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy;

interface ColorPickerStrategy
{
    public function pickColor(Image $image, int $x, int $y): Color;
}