<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy;

class Color
{
    public int $r;
    public int $g;
    public int $b;

    public function __construct($r, $g, $b)
    {
        $this->r = $r;
        $this->g = $g;
        $this->b = $b;
    }

    public function toString(): string
    {
        return '[' . $this->r . ',' . $this->g . ',' . $this->b . ']';
    }
}