<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy;

use App\DesignPatterns\Behavioral\Strategy\Color;
use App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy;
use App\DesignPatterns\Behavioral\Strategy\Image;

class ColorPickerDefault implements ColorPickerStrategy
{
    public function pickColor(Image $image, int $x, int $y): Color
    {
        if ($image->getPixel($x, $y) !== null) {
            return $image->getPixel($x, $y)->getColor();
        }

        return new Color(0, 0, 0);
    }
}