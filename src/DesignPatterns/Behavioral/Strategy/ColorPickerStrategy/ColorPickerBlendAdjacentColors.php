<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy;

use App\DesignPatterns\Behavioral\Strategy\Color;
use App\DesignPatterns\Behavioral\Strategy\ColorPickerStrategy;
use App\DesignPatterns\Behavioral\Strategy\Image;

class ColorPickerBlendAdjacentColors implements ColorPickerStrategy
{
    public function pickColor(Image $image, int $x, int $y): Color
    {
        $colors = array();
        if ($image->getPixel($x, $y - 1) !== null) {
            $colors[] = $image->getPixel($x, $y - 1)->getColor();
        }

        if ($image->getPixel($x, $y + 1) !== null) {
            $colors[] = $image->getPixel($x, $y + 1)->getColor();
        }

        if ($image->getPixel($x - 1, $y) !== null) {
            $colors[] = $image->getPixel($x - 1, $y)->getColor();
        }

        if ($image->getPixel($x + 1, $y) !== null) {
            $colors[] = $image->getPixel($x + 1, $y)->getColor();
        }

        if ($image->getPixel($x, $y) !== null) {
            $colors[] = $image->getPixel($x, $y)->getColor();
        }

        $newR = $newG = $newB = 0;

        foreach($colors as $color) {
            $newR += $color->r;
            $newG += $color->g;
            $newB += $color->b;
        }

        $count = count($colors);

        $newR /= $count;
        $newG /= $count;
        $newB /= $count;

        return new Color((int) round($newR), (int) round($newG), (int) round($newB));
    }
}