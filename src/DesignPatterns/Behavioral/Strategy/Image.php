<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Strategy;

class Image
{
    private int $w = 10;

    private int $h = 10;

    /** @var Pixel[] */
    private array $pixels;

    public function __construct()
    {
        $numberOfPixels = $this->w * $this->h;
        for ($i = 0; $i < $numberOfPixels; $i++) {
            $row = $i % $this->w;
            $column = ($i - $row) / $this->h;
            $this->pixels[] = new Pixel(
                $row, $column, new Color(random_int(0, 255), random_int(0, 255), random_int(0, 255))
            );
        }
    }

    public function getPixel($x, $y): ?Pixel
    {
        return $this->pixels[$this->h * $y + $x] ?? null;
    }

    public function getWidth(): int
    {
        return $this->w;
    }

    public function getHeight(): int
    {
        return $this->h;
    }
}
