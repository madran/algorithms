<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\State;

abstract class UnitState
{
    protected int $bonusDefense = 0;
    protected int $bonusFieldOfView = 0;
    protected string $stateName = '';

    public function getBonusDefense(): int
    {
        return $this->bonusDefense;
    }

    public function getBonusFieldOfView(): int
    {
        return $this->bonusFieldOfView;
    }

    public function getStateName(): string
    {
        return $this->stateName;
    }
}