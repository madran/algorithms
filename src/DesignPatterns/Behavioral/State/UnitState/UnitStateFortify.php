<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\State\UnitState;

use App\DesignPatterns\Behavioral\State\UnitState;

class UnitStateFortify extends UnitState
{
    public function __construct()
    {
        $this->stateName = 'fortify';
        $this->bonusDefense = 3;
    }
}