<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\State\UnitState;

use App\DesignPatterns\Behavioral\State\UnitState;

class UnitStateSentry extends UnitState
{
    public function __construct()
    {
        $this->stateName = 'sentry';
        $this->bonusFieldOfView = 2;
    }
}