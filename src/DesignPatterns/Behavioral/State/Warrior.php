<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\State;

class Warrior
{
    private int $defense = 5;
    private int $fieldOfView = 3;

    private ?UnitState $currentState = null;

    public function changeState(UnitState $state):void
    {
        $this->currentState = $state;
    }

    public function setDefaultState(): void
    {
        $this->currentState = null;
    }

    public function getDefense(): int
    {
        if ($this->currentState !== null) {
            return $this->defense + $this->currentState->getBonusDefense();
        }

        return $this->defense;
    }

    public function getFieldOfView(): int
    {
        if ($this->currentState !== null) {
            return $this->fieldOfView + $this->currentState->getBonusFieldOfView();
        }

        return $this->fieldOfView;
    }

    public function toString(): string
    {
        if ($this->currentState === null) {
            return 'Stan domyślny: obrona=' . $this->getDefense() . ' | zasięg widzenia=' . $this->getFieldOfView() . PHP_EOL;
        }

        return 'Stan ' . $this->currentState->getStateName() .
            ': obrona=' . $this->getDefense() .
            ' | zasięg widzenia=' . $this->getFieldOfView() . PHP_EOL;
    }
}
