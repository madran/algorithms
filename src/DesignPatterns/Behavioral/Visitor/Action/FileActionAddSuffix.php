<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Visitor\Action;

use App\DesignPatterns\Behavioral\Visitor\Action;
use App\DesignPatterns\Behavioral\Visitor\File\BaseFile;
use App\DesignPatterns\Behavioral\Visitor\File\ImageFile;

class FileActionAddSuffix implements Action
{
    public string $prefix;

    public function __construct(string $prefix)
    {
        $this->prefix = $prefix;
    }

    public function execute(BaseFile $file): void
    {
        if ($file instanceof ImageFile) {
            $fileNameArray = explode('.', $file->getName());
            $size = count($fileNameArray);
            $fileExtension = $fileNameArray[$size - 1];
            $fileNameArray[$size - 1] = $this->prefix;
            $file->setName(implode('', $fileNameArray) . '.' . $fileExtension);
        }
    }
}