<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Visitor\File;

use App\DesignPatterns\Behavioral\Visitor\Action;

class ImageFile extends BaseFile
{
    public function action(Action $action): void
    {
        $action->execute($this);
    }
}