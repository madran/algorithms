<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Visitor\File;

class Directory extends BaseFile
{
    /** @var BaseFile[] */
    private array $children;

    public function addElement(BaseFile $child)
    {
        $this->children[] = $child;
    }

    public function action($action): void
    {
        foreach ($this->children as $child) {
            $child->action($action);
        }
        $action->execute($this);
    }

    /**
     * @return BaseFile[]
     */
    public function getElements(): array
    {
        return $this->children;
    }
}