<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Visitor\File;

use App\DesignPatterns\Behavioral\Visitor\Action;

abstract class BaseFile
{
    protected string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    abstract public function action(Action $action): void;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}