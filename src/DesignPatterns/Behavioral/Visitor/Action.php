<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Visitor;

use App\DesignPatterns\Behavioral\Visitor\File\BaseFile;

interface Action
{
    public function execute(BaseFile $file): void;
}