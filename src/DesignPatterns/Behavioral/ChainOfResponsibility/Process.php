<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\ChainOfResponsibility;

abstract class Process
{
    private ?Process $nextProcess = null;

    public function addNextProcess($nextProcess): void
    {
        $this->nextProcess = $nextProcess;
    }

    public function start(Request $request): ?Response
    {
        $result = null;

        if ($this->canProcess($request)) {
            $result = $this->process($request);
        }

        if($result === null && $this->nextProcess !== null) {
            $result = $this->nextProcess->start($request);
        }

        return $result;
    }

    abstract protected function process(Request $request): ?Response;

    abstract protected function canProcess(Request $request): bool;
}