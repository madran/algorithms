<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\ChainOfResponsibility\ReturnValueProcess;

use App\DesignPatterns\Behavioral\ChainOfResponsibility\Request;
use App\DesignPatterns\Behavioral\ChainOfResponsibility\Response;
use App\DesignPatterns\Behavioral\ChainOfResponsibility\Process;

class ProcessC extends Process
{
    protected function process(Request $request): ?Response
    {
        echo 'process C' . PHP_EOL;
        return new Response(12);
    }

    protected function canProcess(Request $request): bool
    {
        echo 'can process C' . PHP_EOL;
        return $request->getValue() === 12;
    }
}