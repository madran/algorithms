<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Memento;

class Player
{
    public int $health = 100;
    public int $stamina = 25;
    public int $food = 100;
    public int $water = 100;

    public function __construct(PlayerStatus $playerStatus = null)
    {
        if($playerStatus !== null) {
            $this->health = $playerStatus->health;
            $this->stamina = $playerStatus->stamina;
            $this->food = $playerStatus->food;
            $this->water = $playerStatus->water;
        }
    }

    public function saveStatus(): PlayerStatus
    {
        return new PlayerStatus($this);
    }

    public function toString(): string
    {
        return  'Health: ' . $this->health . PHP_EOL .
            'Stamina: ' . $this->stamina . PHP_EOL .
            'Food: ' . $this->food . PHP_EOL .
            'Water: ' . $this->water;
    }
}