<?php
declare(strict_types=1);

namespace App\DesignPatterns\Behavioral\Memento;

class PlayerStatus
{
    public int $health;
    public int $stamina;
    public int $food;
    public int $water;

    public function __construct(Player $player)
    {
        $this->health = $player->health;
        $this->stamina = $player->stamina;
        $this->food = $player->food;
        $this->water = $player->water;
    }
}