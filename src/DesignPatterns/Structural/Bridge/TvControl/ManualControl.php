<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Bridge\TvControl;

use App\DesignPatterns\Structural\Bridge\TvControl;

class ManualControl implements TvControl
{
    public function changeChannel(int $channel): void
    {
        echo 'Manual: ';
    }

    public function off(): void
    {
        echo 'Manual: ';
    }

    public function on(): void
    {
        echo 'Manual: ';
    }
}