<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Bridge\TvControl;

use App\DesignPatterns\Structural\Bridge\TvControl;

class RemoteControl implements TvControl
{
    public function changeChannel(int $channel): void
    {
        echo 'Remote: ';
    }

    public function off(): void
    {
        echo 'Remote: ';
    }

    public function on(): void
    {
        echo 'Remote: ';
    }
}