<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Bridge;

interface TvControl
{
    public function on() :void;
    public function off() :void;
    public function changeChannel(int $channel) :void;
}