<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Bridge;

interface Tv
{
    public function setControlSystem(TvControl $controlSystem) :void;
    public function on() :void;
    public function off() :void;
    public function changeChannel(int $channel) :void;
}