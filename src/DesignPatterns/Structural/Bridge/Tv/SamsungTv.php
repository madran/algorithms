<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Bridge\Tv;

use App\DesignPatterns\Structural\Bridge\Tv;
use App\DesignPatterns\Structural\Bridge\TvControl;

class SamsungTv implements Tv
{
    private TvControl $controlSystem;

    public function setControlSystem(TvControl $controlSystem) :void
    {
        $this->controlSystem = $controlSystem;
    }

    public function changeChannel(int $channel): void
    {
        $this->controlSystem->changeChannel($channel);
        echo 'samsung ' . $channel . PHP_EOL;
    }

    public function off(): void
    {
        $this->controlSystem->off();
        echo 'samsung off' . PHP_EOL;
    }

    public function on(): void
    {
        $this->controlSystem->on();
        echo 'samsung on' . PHP_EOL;
    }
}