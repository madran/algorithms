<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Facade\Shop;

class Payment
{
    public function setPaymentMethod(string $paymentMethod): void {}

    public function checkout(): bool
    {
        return true;
    }
}