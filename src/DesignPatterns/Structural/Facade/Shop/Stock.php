<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Facade\Shop;

class Stock
{
    public function findProduct(string $product): Product
    {
        return new Product();
    }

    /**
     * @param string $category
     * @return Product[]
     */
    public function getProductsByCategory(string $category): array
    {
        return [new Product()];
    }
}