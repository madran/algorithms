<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Facade\Shop;

class Product
{
    private int $id;
    private string $name;
}