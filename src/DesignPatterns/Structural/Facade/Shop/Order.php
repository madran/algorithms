<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Facade\Shop;

class Order
{
    public function setProducts(ShoppingCart $shoppingCart): void {}

    public function getSummary(): OrderSummary
    {
        return new OrderSummary();
    }
}