<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Facade;

use App\DesignPatterns\Structural\Facade\Shop\Order;
use App\DesignPatterns\Structural\Facade\Shop\OrderSummary;
use App\DesignPatterns\Structural\Facade\Shop\Payment;
use App\DesignPatterns\Structural\Facade\Shop\Product;
use App\DesignPatterns\Structural\Facade\Shop\ShoppingCart;
use App\DesignPatterns\Structural\Facade\Shop\Stock;

class Shop
{
    private Stock $stock;
    private Payment $payment;
    private Order$order;

    public function __construct(Stock $stock, Payment $payment, Order $order)
    {
        $this->stock = $stock;
        $this->payment = $payment;
        $this->order = $order;
    }

    public function findProduct(string $name): Product
    {
        return $this->stock->findProduct($name);
    }

    /**
     * @param string $category
     * @return Product[]
     */
    public function getProductsByCategory(string $category): array
    {
        return $this->stock->getProductsByCategory($category);
    }


    public function order(ShoppingCart $shoppingCart): OrderSummary
    {
        $this->order->setProducts($shoppingCart);

        return $this->order->getSummary();
    }

    public function checkout(string $paymentMethod): bool
    {
        $this->payment->setPaymentMethod($paymentMethod);
        return $this->payment->checkout();
    }
}