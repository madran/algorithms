<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy;

class Credentials
{
    public function isValid(): bool
    {
        return true;
    }
}