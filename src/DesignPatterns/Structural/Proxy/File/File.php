<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy\File;

class File extends FileBase
{
    protected string $name;
    protected int $size;

    private string $content;

    public function __construct(string $path)
    {
        parent::__construct($path);

        preg_match('/\w+(?:\.\w+)*$/', $path, $matches, PREG_OFFSET_CAPTURE);
        $this->name = $matches[0][0];

        $this->loadContent();
    }

    public function show(): void
    {
        echo 'View file: ' . $this->name . PHP_EOL;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    protected function loadContent(): void
    {
        $this->content = 'Some large content';
    }
}