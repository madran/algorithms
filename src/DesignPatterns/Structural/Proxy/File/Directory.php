<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy\File;

class Directory extends FileBase
{
    /** @var File[] */
    private $files = [];

    public function __construct($path)
    {
        parent::__construct($path);
    }

    protected function loadContent(): void
    {
        echo 'Load directory and file data' . PHP_EOL;

        $this->files[] = new File('C:\Windows\file1.txt');
        $this->files[] = new File('C:\Windows\file2.txt');
        $this->files[] = new File('C:\Windows\file3.txt');
    }

    public function show(): void
    {
        foreach ($this->files as $file)
        {
            echo $file->getContent();
        }
    }

    public function getContent(): string
    {
        $content = '';

        foreach ($this->files as $file)
        {
            $content .= $file->getContent();
        }

        return $content;
    }
}