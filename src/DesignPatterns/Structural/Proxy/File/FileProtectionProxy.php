<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy\File;

use App\DesignPatterns\Structural\Proxy\Credentials;
use Exception;

class FileProtectionProxy extends FileBase
{
    private ?File $file = null;

    private Credentials $credentials;

    public function __construct(string $path, Credentials $credentials)
    {
        parent::__construct($path);
        $this->credentials = $credentials;
    }

    public function show(): void
    {
        if (!$this->credentials->isValid()) {
            throw new Exception();
        }

        if ($this->file === null) {
            $this->file = new File($this->path);
        }

        $this->file->show();
    }

    public function getContent(): string
    {
        if (!$this->credentials->isValid()) {
            throw new Exception();
        }

        if ($this->file === null) {
            $this->file = new File($this->path);
        }

        return $this->file->getContent();
    }
}