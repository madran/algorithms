<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy\File;

use App\DesignPatterns\Structural\Proxy\FileInterface;

abstract class FileBase implements FileInterface
{
    protected string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function delete(): void {}

    public function move(string $newPath): void {}

    public function copy(string $newPath): void {}
}