<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy\File;

class FileVirtualProxy extends FileBase
{
    private ?File $file = null;

    public function __construct(string $path)
    {
        parent::__construct($path);
    }

    public function show(): void
    {
        if ($this->file === null) {
            $this->file = new File($this->path);
        }

        $this->file->show();
    }

    public function getContent(): string
    {
        if ($this->file === null) {
            $this->file = new File($this->path);
        }

        return $this->file->getContent();
    }
}