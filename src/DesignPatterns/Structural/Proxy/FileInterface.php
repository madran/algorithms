<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Proxy;

interface FileInterface
{
    public function delete(): void;

    public function move(string $newPath): void;

    public function copy(string $newPath): void;

    public function show(): void;

    public function getContent(): string;
}