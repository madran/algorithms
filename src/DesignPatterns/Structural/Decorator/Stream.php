<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Decorator;

abstract class Stream
{
    protected string $data = '';

    public function add(string $data) :void
    {
        $this->data .= $data;
    }

    public function setData(string $data) :void
    {
        $this->data = $data;
    }

    public function getData() :string
    {
        return $this->data;
    }

    abstract public function save() :void;
}