<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Decorator\Stream\StreamMode;

use App\DesignPatterns\Structural\Decorator\Stream\StreamDecorator;

class AsciiStream extends StreamDecorator
{
    public function save() :void
    {
        $data = $this->stream->getData();
        $asciiData = '';

        foreach ($this->stringIterate($data) as $char) {
            $asciiData .= strval(ord($char));
        }

        $this->stream->setData($asciiData);
        $this->stream->save();
    }

    private function stringIterate($str)
    {
        $length = mb_strlen($str);

        for ($i = 0; $i < $length; $i++) {
            yield mb_substr($str, $i, 1);
        }
    }
}