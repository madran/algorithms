<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Decorator\Stream\StreamMode;

use App\DesignPatterns\Structural\Decorator\Stream\StreamDecorator;

class CompressStream extends StreamDecorator
{
    public function save() :void
    {
        $compressedData = gzcompress($this->stream->getData());
        $this->stream->setData($compressedData);
        $this->stream->save();
    }
}