<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Decorator\Stream;

use App\DesignPatterns\Structural\Decorator\Stream;

abstract class StreamDecorator extends Stream
{
    protected Stream $stream;

    public function __construct(Stream $stream)
    {
        $this->stream = $stream;
    }

    public function setData(string $data) :void
    {
        $this->stream->setData($data);
    }

    public function add(string $data) :void
    {
        $this->stream->add($data);
    }

    public function getData() :string
    {
        return $this->stream->getData();
    }

    public function save(): void
    {
        $this->stream->save();
    }
}