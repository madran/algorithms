<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Decorator\Stream;

use App\DesignPatterns\Structural\Decorator\Stream;

class FileStream extends Stream
{
    private string $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function save(): void
    {
        file_put_contents($this->fileName, $this->data);
    }
}