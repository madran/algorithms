<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Adapter\Authentication;

use App\DesignPatterns\Structural\Adapter\Authentication;

class UserAuthentication implements Authentication
{
    private bool $isAuthenticated = false;

    public function authenticate(string $userLogin, string $password) : bool
    {
        echo 'Standard authenticate user: ' . $userLogin;

        $this->isAuthenticated = true;
        return true;
    }

    public function isAuthenticated() : bool
    {
        return $this->isAuthenticated;
    }
}