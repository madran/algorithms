<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Adapter\Authentication;

class GoogleUserAuthentication
{
    private string $login;
    private string $password;

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function authenticateUser() : bool
    {
        echo 'Google authenticate user: ' . $this->login . ' password: ' . $this->password;
        return true;
    }
}