<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Adapter\Authentication;

use App\DesignPatterns\Structural\Adapter\Authentication;

class GoogleUserAuthenticationAdapter implements Authentication
{
    private GoogleUserAuthentication $googleUserAuthentication;

    private bool $isAuthenticated = false;

    public function __construct(GoogleUserAuthentication $googleUserAuthentication)
    {
        $this->googleUserAuthentication = $googleUserAuthentication;
    }

    public function authenticate(string $userLogin, string $password) : bool
    {
        $this->googleUserAuthentication->setLogin($userLogin);
        $this->googleUserAuthentication->setPassword($password);
        $this->isAuthenticated = $this->googleUserAuthentication->authenticateUser();
        return $this->isAuthenticated;
    }

    public function isAuthenticated() : bool
    {
        return $this->isAuthenticated;
    }
}