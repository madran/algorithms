<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Adapter;

class FileManager
{
    private Authentication $authenticationManager;

    public function __construct(Authentication $authenticationManager)
    {
        $this->authenticationManager = $authenticationManager;
    }

    public function getRecentFileContent(): string
    {
        if ($this->authenticationManager->isAuthenticated()) {
            return 'File content.';
        }

        return '';
    }
}