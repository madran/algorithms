<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Adapter;

interface Authentication
{
    public function authenticate(string $userLogin, string $password): bool;
    public function isAuthenticated(): bool;
}