<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Flyweight\Document;

use App\DesignPatterns\Structural\Flyweight\CharacterFactory;
use App\DesignPatterns\Structural\Flyweight\Document;
use Generator;

class DocumentWithFlyweight extends Document
{
    public function __construct(string $text, CharacterFactory $characterFactory)
    {
        foreach ($this->stringIterate($text) as $char) {
            $this->characters[] = $characterFactory->getCharacter($char);
        }
    }

    private function stringIterate(string $str): Generator
    {
        $length = mb_strlen($str);

        for ($i = 0; $i < $length; $i++) {
            yield mb_substr($str, $i, 1);
        }
    }
}