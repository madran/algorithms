<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Flyweight\Document;

use App\DesignPatterns\Structural\Flyweight\Character;
use App\DesignPatterns\Structural\Flyweight\Document;
use Generator;

class DocumentWithoutFlyweight extends Document
{
    public function __construct(string $text)
    {
        foreach ($this->stringIterate($text) as $char) {
            $this->characters[] = new Character($char);
        }
    }

    private function stringIterate($str): Generator
    {
        $length = mb_strlen($str);

        for ($i = 0; $i < $length; $i++) {
            yield mb_substr($str, $i, 1);
        }
    }
}