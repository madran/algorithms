<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Flyweight;

abstract class Document
{
    /** @var Character[] */
    protected array $characters = [];

    public function toString(): string
    {
        $text = '';

        foreach ($this->characters as $char) {
            $text .= $char->getChar();
        }

        return $text;
    }

    public function countValues()
    {
        $newChars = array_map(
            static function($obj) {
                return spl_object_hash($obj);
            },
            $this->characters
        );

        return count(array_unique($newChars));
    }
}