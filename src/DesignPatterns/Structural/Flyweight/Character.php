<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Flyweight;

class Character
{
    private string $char;

    public function __construct(string $char)
    {
        $this->char = $char;
    }

    public function getChar(): string
    {
        return $this->char;
    }
}