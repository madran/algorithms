<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Flyweight;

class CharacterFactory
{
    /** @var Character[] */
    private array $characters = [];

    public function getCharacter(string $char) : Character
    {
        foreach ($this->characters as $objChar) {
            if ($objChar->getChar() === $char) {
                return $objChar;
            }
        }

        $newChar = new Character($char);
        $this->characters[] = $newChar;
        return $newChar;
    }
}