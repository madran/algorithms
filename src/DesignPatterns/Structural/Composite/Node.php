<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Composite;

interface Node
{
    public function getValue() :int;
}