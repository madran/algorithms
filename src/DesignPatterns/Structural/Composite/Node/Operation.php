<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Composite\Node;

use App\DesignPatterns\Structural\Composite\Node;

abstract class Operation implements Node
{
    protected Node $left;
    protected Node $right;
    protected Node $value;

    public function setLeft(Node $left) :void
    {
        $this->left = $left;
    }

    public function setRgiht(Node $right) :void
    {
        $this->right = $right;
    }
}