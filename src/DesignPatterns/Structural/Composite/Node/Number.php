<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Composite\Node;

use App\DesignPatterns\Structural\Composite\Node;

class Number implements Node
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue() :int
    {
        return $this->value;
    }
}