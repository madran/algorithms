<?php
declare(strict_types=1);

namespace App\DesignPatterns\Structural\Composite\Node\Operation;

use App\DesignPatterns\Structural\Composite\Node\Operation;

class Addition extends Operation
{
    public function getValue() :int
    {
        return $this->left->getValue() + $this->right->getValue();
    }
}