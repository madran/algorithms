<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate;

use App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate;

class StatementDocumentTemplate extends DocumentTemplate
{
    public function fillTemplate(array $data): void
    {

    }
}