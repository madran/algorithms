<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate;

use App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate;

class ProposalDocumentTemplate extends DocumentTemplate
{
    public function fillTemplate(array $data): void
    {
        $user = $data['user'];

        $this->documentView =
            $user->getFirstName() . ' ' . $user->getLastName() . PHP_EOL
            . $user->getAddress()->getStreetName() . ' ' . $user->getAddress()->getHouseNumber() . ' ' . $user->getAddress()->getFlatNumber() . PHP_EOL
            . $user->getAddress()->getCity() . PHP_EOL
            . PHP_EOL
            . PHP_EOL
            . 'Wniosek'
            . PHP_EOL
            . 'Bla bla bla że wniosek.'
            . PHP_EOL;
    }
}