<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod\Document;

use App\DesignPatterns\Creational\FactoryMethod\Document;
use App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate;
use App\DesignPatterns\Creational\FactoryMethod\DocumentTemplate\ProposalDocumentTemplate;

class ProposalDocument extends Document
{
    protected function getDocumentTemplate(): DocumentTemplate
    {
        return new ProposalDocumentTemplate();
    }
}