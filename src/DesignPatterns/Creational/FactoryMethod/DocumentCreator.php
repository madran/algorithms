<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod;

use App\DesignPatterns\Creational\FactoryMethod\Document\ProposalDocument;
use App\DesignPatterns\Creational\FactoryMethod\Document\StatementDocument;
use Exception;

class DocumentCreator
{
    public function createDocument(string $type, array $data): Document
    {
        switch ($type) {
            case 'proposal':
                $proposalDocument = new ProposalDocument();
                return $proposalDocument->generateDocument($data);
            case 'statament':
                $statementDocument = new StatementDocument();
                return $statementDocument->generateDocument($data);
            default:
                throw new Exception('Bad document type!');
        }
    }
}