<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod;

class Address
{
    private string $city;
    private string $streetName;
    private string $houseNumber;
    private string $flatNumber;

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): void
    {
        $this->streetName = $streetName;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    public function getFlatNumber(): string
    {
        return $this->flatNumber;
    }

    public function setFlatNumber(string $flatNumber): void
    {
        $this->flatNumber = $flatNumber;
    }
}