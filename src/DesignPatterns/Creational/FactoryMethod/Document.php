<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod;

abstract class Document
{
    protected DocumentTemplate $documentTemplate;

    public function generateDocument($data): self
    {
        $this->documentTemplate = $this->getDocumentTemplate();

        $this->documentTemplate->fillTemplate($data);

        return $this;
    }

    public function toString(): string
    {
        return $this->documentTemplate->getDocumentView();
    }

    abstract protected function getDocumentTemplate(): DocumentTemplate;
}