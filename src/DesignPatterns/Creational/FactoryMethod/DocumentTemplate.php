<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\FactoryMethod;

abstract class DocumentTemplate
{
    protected string $documentView = '';

    public function getDocumentView(): string
    {
        return $this->documentView;
    }

    abstract public function fillTemplate(array $data);
}