<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\ObjectPool;

class Warrior
{
    public int $attack = 8;
    public int $defense = 5;
    public bool $isDead = false;
    public bool $isVeteran = false;

    public function __toString(): string
    {
        return spl_object_hash($this);
    }
}