<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\ObjectPool;

class Barracks
{
    private static array $waitingWarriors = [];

    private static array $fightingWarriors = [];

    public function getWarrior(): Warrior
    {
        if (empty(self::$waitingWarriors)) {
            $warrior = new Warrior();
            self::$fightingWarriors[] = $warrior;
            return $warrior;
        }

        $warrior = reset(self::$waitingWarriors);
        self::$waitingWarriors = array_diff(self::$waitingWarriors, array($warrior));
        self::$fightingWarriors[] = $warrior;
        return $warrior;
    }

    public function quarter(Warrior $warrior): void
    {
        $warrior->isVeteran = true;
        self::$waitingWarriors[] = $warrior;
        if (($key = array_search($warrior, self::$fightingWarriors)) !== false) {
            unset(self::$fightingWarriors[$key]);
        }
    }

    public function clearBarracks(): void
    {
        foreach (self::$fightingWarriors as $warrior) {
            if ($warrior->isDead) {
                self::$fightingWarriors = array_diff(self::$fightingWarriors, array($warrior));
            }
        }

        foreach (self::$waitingWarriors as $warrior) {
            if ($warrior->isDead) {
                self::$waitingWarriors = array_diff(self::$waitingWarriors, array($warrior));
            }
        }
    }
}