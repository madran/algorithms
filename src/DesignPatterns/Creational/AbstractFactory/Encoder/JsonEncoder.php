<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\Encoder;

use App\DesignPatterns\Creational\AbstractFactory\Encoder;

class JsonEncoder implements Encoder
{
    public function encode(array $data): string
    {
        return json_encode($data);
    }
}