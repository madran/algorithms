<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\Encoder;

use App\DesignPatterns\Creational\AbstractFactory\Encoder;
use RuntimeException;
use SimpleXMLElement;

class XmlEncoder implements Encoder
{
    public function encode(array $data) :string
    {
        if (count($data) === 1) {
            $firstKey = array_key_first($data);
            $rootElement = '<' . $firstKey . '/>';
            $data = $data[$firstKey];
        } else {
            $rootElement = '<root/>';
        }

        $xml = new SimpleXMLElement($rootElement);

        $this->recursiveEncode($xml, $data);

        $result = $xml->asXML();

        if (is_string($result)) {
            return $result;
        }

        throw new RuntimeException('XML encode failed.');
    }

    private function recursiveEncode(SimpleXMLElement $xml, array $data): SimpleXMLElement
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->recursiveEncode($xml->addChild((string) $key), $value);
            } else {
                $xml->addChild((string) $key, (string) $value);
            }
        }

        return $xml;
    }
}