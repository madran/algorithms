<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory;

interface Decoder
{
    public function decode(string $data): array;
}