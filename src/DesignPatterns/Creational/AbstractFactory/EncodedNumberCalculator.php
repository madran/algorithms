<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory;

class EncodedNumberCalculator
{
    public function subtract(string $data, int $number, ConverterFactory $factory): string
    {
        $decoder = $factory->getDecoder();

        $decodedData = $decoder->decode($data);

        $decodedData['first'] -= $number;
        $decodedData['second'] -= $number;
        $decodedData['third'] -= $number;

        $encoder = $factory->getEncoder();

        return $encoder->encode($decodedData);
    }
}