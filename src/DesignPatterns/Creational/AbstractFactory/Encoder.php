<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory;

interface Encoder
{
    public function encode(array $data) :string;
}