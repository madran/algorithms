<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\ConverterFactory;

use App\DesignPatterns\Creational\AbstractFactory\ConverterFactory;
use App\DesignPatterns\Creational\AbstractFactory\Decoder;
use App\DesignPatterns\Creational\AbstractFactory\Decoder\XmlDecoder;
use App\DesignPatterns\Creational\AbstractFactory\Encoder;
use App\DesignPatterns\Creational\AbstractFactory\Encoder\XmlEncoder;

class XmlConverterFactory implements ConverterFactory
{

    public function getDecoder(): Decoder
    {
        return new XmlDecoder();
    }

    public function getEncoder(): Encoder
    {
        return new XmlEncoder();
    }
}