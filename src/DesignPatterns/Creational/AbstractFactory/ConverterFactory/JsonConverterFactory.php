<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\ConverterFactory;

use App\DesignPatterns\Creational\AbstractFactory\ConverterFactory;
use App\DesignPatterns\Creational\AbstractFactory\Decoder;
use App\DesignPatterns\Creational\AbstractFactory\Decoder\JsonDecoder;
use App\DesignPatterns\Creational\AbstractFactory\Encoder;
use App\DesignPatterns\Creational\AbstractFactory\Encoder\JsonEncoder;

class JsonConverterFactory implements ConverterFactory
{
    public function getDecoder(): Decoder
    {
        return new JsonDecoder();
    }

    public function getEncoder(): Encoder
    {
        return new JsonEncoder();
    }
}