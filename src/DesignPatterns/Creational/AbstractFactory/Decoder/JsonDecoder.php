<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\Decoder;

use App\DesignPatterns\Creational\AbstractFactory\Decoder;

class JsonDecoder implements Decoder
{
    public function decode(string $data): array
    {
        return json_decode($data, true);
    }
}