<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory\Decoder;

use App\DesignPatterns\Creational\AbstractFactory\Decoder;

class XmlDecoder implements Decoder
{
    public function decode(string $data): array
    {
        $xml = simplexml_load_string($data);
        $json = json_encode($xml);
        return json_decode($json,true);
    }
}