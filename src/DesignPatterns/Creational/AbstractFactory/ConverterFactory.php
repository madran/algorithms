<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\AbstractFactory;

interface ConverterFactory
{
    public function getDecoder(): Decoder;
    public function getEncoder(): Encoder;
}