<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

interface WindowBuilderDirector
{
    public function create(WindowBuilder $builder): Window;
}