<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

class Window
{
    private int $width;

    private int $height;

    private Border $border;

    /** @var string[] */
    private array $fileNames = [];

    private array $components;

    public function __construct(int $width, int $height, Border $border, array $components = [])
    {
        $this->width = $width;
        $this->height = $height;
        $this->border = $border;
        $this->components = $components;
    }

    public function setFileNames(array $fileNames) :void
    {
        $this->fileNames = $fileNames;
    }

    public function render() :string
    {
        $window[] = $this->border->getTopLeftCorner()
            . str_repeat($this->border->getHorizontalBorder(), $this->width - 2)
            . $this->border->getTopRightCorner()
            . PHP_EOL;

        for ($y = 2, $i = 0; $y < $this->height; $y++, $i++) {
            $window[] = $this->border->getVerticalBorder()
                . ' '
                . (isset($this->fileNames[$i]) ? $this->fileNames[$i] : '')
                . str_repeat(' ', $this->width - 2 - mb_strlen(isset($this->fileNames[$i]) ? $this->fileNames[$i] : '') - 1)
                . $this->border->getVerticalBorder()
                . PHP_EOL;
        }

        $window[] = $this->border->getBottomLeftCorner()
            . str_repeat($this->border->getHorizontalBorder(), $this->width - 2)
            . $this->border->getBottomRightCorner()
            . PHP_EOL;

        $window = $this->renderComponents($window);

        return implode('', $window);
    }

    private function renderComponents(array $window) :array
    {
        /**
         * @var Component $component
         * @var string[] $options
         */
        foreach ($this->components as [$component, $options]) {

            if ($options['position']['vertical'] === Component::POSITION_TOP) {
                $row = 0;
            } else {
                $row = $this->height - 1;
            }

            if ($options['position']['horizontal'] === Component::POSITION_LEFT) {
                $column = 2;
            } else {
                $column = $this->width - $component->getLength() - 2;
            }

            $window[$row] = $this->mb_substr_replace($window[$row], $component->getView(), $column, $component->getLength());
        }

        return $window;
    }

    private function mb_substr_replace($string, $replacement, $start, $length = null, $encoding = null): string
    {
        $string_length = (is_null($encoding) === true) ? mb_strlen($string) : mb_strlen($string, $encoding);

        if ($start < 0) {
            $start = max(0, $string_length + $start);
        } else if ($start > $string_length) {
            $start = $string_length;
        }

        if ($length < 0) {
            $length = max(0, $string_length - $start + $length);
        } else if ((is_null($length) === true) || ($length > $string_length)) {
            $length = $string_length;
        }

        if (($start + $length) > $string_length) {
            $length = $string_length - $start;
        }

        if (is_null($encoding) === true) {
            return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length);
        }

        return mb_substr($string, 0, $start, $encoding) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length, $encoding);
    }
}