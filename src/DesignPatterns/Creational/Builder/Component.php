<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

abstract class Component
{
    public const POSITION_TOP = 'top';
    public const POSITION_BOTTOM = 'bottom';
    public const POSITION_LEFT = 'left';
    public const POSITION_RIGHT = 'right';

    protected string $view = '';
    protected int $length = 0;

    abstract protected function render() :string;

    public function getView() :string
    {
        $this->renderComponent();
        return $this->view;
    }

    public function getLength() :int
    {
        $this->renderComponent();
        return $this->length;
    }

    private function renderComponent(): void
    {
        if (empty($this->view)) {
            $this->view = $this->render();
            $this->length = mb_strlen($this->view);
        }
    }
}