<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

abstract class WindowBuilder
{
    protected Window $window;

    protected int $width;

    protected int $height;

    protected Border $border;

    protected array $components = [];

    public function __construct(int $width, int $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function addComponent(Component $component, string $vertical, string $horizontal): self
    {
        $this->components[] = [
            $component,
            [
                'position' => [
                    'vertical' => $vertical,
                    'horizontal' => $horizontal,
                ]
            ]
        ];

        return $this;
    }

    public function setBorder(Border $border): self
    {
        $this->border = $border;
        return $this;
    }

    public function build(): Window
    {
        return new Window($this->width, $this->height, $this->border, $this->components);
    }
}