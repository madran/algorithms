<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

use App\DesignPatterns\Creational\Builder\WindowBuilder\WindowSimpleBuilder;
use App\DesignPatterns\Creational\Builder\WindowBuilderDirector\MainWindowDirector;
use App\DesignPatterns\Creational\Builder\WindowBuilderDirector\PopUpWindowDirector;

class WindowApp
{
    public function start()
    {
        $mainWindowDirector = new MainWindowDirector(['A', 'B', 'C'], 'C:/Windows/system');

        $mainWindow = $mainWindowDirector->create(new WindowSimpleBuilder(50, 10));
        $mainWindow->setFileNames(['folder1', 'folder2', 'folder folder3']);
        echo $mainWindow->render();

        echo PHP_EOL . PHP_EOL . PHP_EOL;

        $popUpWindowDirector = new PopUpWindowDirector();

        $popUpWindow = $popUpWindowDirector->create(new WindowSimpleBuilder(10, 3));
        echo $popUpWindow->render();
    }
}