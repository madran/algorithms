<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\Border;

use App\DesignPatterns\Creational\Builder\Border;

class DoubleLineBorder extends Border
{
    public function __construct()
    {
        $this->horizontalBorder = '═';
        $this->verticalBorder = '║';
        $this->topLeftCorner = '╔';
        $this->topRightCorner = '╗';
        $this->bottomLeftCorner = '╚';
        $this->bottomRightCorner = '╝';
    }
}