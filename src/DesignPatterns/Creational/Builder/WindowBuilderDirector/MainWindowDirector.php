<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\WindowBuilderDirector;

use App\DesignPatterns\Creational\Builder\Border\DoubleLineBorder;
use App\DesignPatterns\Creational\Builder\Component;
use App\DesignPatterns\Creational\Builder\Component\CurrentPathComponent;
use App\DesignPatterns\Creational\Builder\Component\DateComponent;
use App\DesignPatterns\Creational\Builder\Component\DriveLetterComponent;
use App\DesignPatterns\Creational\Builder\Window;
use App\DesignPatterns\Creational\Builder\WindowBuilder;
use App\DesignPatterns\Creational\Builder\WindowBuilderDirector;

class MainWindowDirector implements WindowBuilderDirector
{
    private array $letters;

    private string $path;

    public function __construct(array $letters, string $path)
    {
        $this->letters = $letters;
        $this->path = $path;
    }

    public function create(WindowBuilder $builder): Window
    {
        $driverComponent = new DriveLetterComponent($this->letters);
        $dateComponent = new DateComponent();
        $currentPathComponent = new CurrentPathComponent($this->path);

        return $builder
            ->setBorder(new DoubleLineBorder())
            ->addComponent($driverComponent, Component::POSITION_BOTTOM, Component::POSITION_LEFT)
            ->addComponent($dateComponent, Component::POSITION_BOTTOM, Component::POSITION_RIGHT)
            ->addComponent($currentPathComponent, Component::POSITION_TOP, Component::POSITION_LEFT)
            ->build();
    }
}