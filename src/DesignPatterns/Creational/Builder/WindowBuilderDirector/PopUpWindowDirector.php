<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\WindowBuilderDirector;

use App\DesignPatterns\Creational\Builder\Border\SingleLineBorder;
use App\DesignPatterns\Creational\Builder\Window;
use App\DesignPatterns\Creational\Builder\WindowBuilder;
use App\DesignPatterns\Creational\Builder\WindowBuilderDirector;

class PopUpWindowDirector implements WindowBuilderDirector
{
    public function create(WindowBuilder $builder): Window
    {
        return $builder
            ->setBorder(new SingleLineBorder())
            ->build();
    }
}