<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder;

abstract class Border
{
    protected string $horizontalBorder;
    protected string $verticalBorder;
    protected string $topLeftCorner;
    protected string $topRightCorner;
    protected string $bottomLeftCorner;
    protected string $bottomRightCorner;

    public function getHorizontalBorder() :string
    {
        return $this->horizontalBorder;
    }

    public function getVerticalBorder() :string
    {
        return $this->verticalBorder;
    }

    public function getTopLeftCorner() :string
    {
        return $this->topLeftCorner;
    }

    public function getTopRightCorner() :string
    {
        return $this->topRightCorner;
    }

    public function getBottomLeftCorner() :string
    {
        return $this->bottomLeftCorner;
    }

    public function getBottomRightCorner() :string
    {
        return $this->bottomRightCorner;
    }
}