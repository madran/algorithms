<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\Component;

use App\DesignPatterns\Creational\Builder\Component;
use DateTime;

class DateComponent extends Component
{
    private DateTime $date;

    public function __construct()
    {
        $this->date = new DateTime();
    }

    protected function render() :string
    {
        return '[ ' . $this->date->format('d-m-Y') . ' ]';
    }
}