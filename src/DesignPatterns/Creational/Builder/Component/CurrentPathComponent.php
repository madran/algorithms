<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\Component;

use App\DesignPatterns\Creational\Builder\Component;

class CurrentPathComponent extends Component
{
    private string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    protected function render() :string
    {
        return '[ ' . $this->path . ' ]';
    }
}