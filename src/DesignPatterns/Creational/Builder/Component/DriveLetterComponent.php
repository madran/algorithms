<?php
declare(strict_types=1);

namespace App\DesignPatterns\Creational\Builder\Component;

use App\DesignPatterns\Creational\Builder\Component;

class DriveLetterComponent extends Component
{
    /** @var string[] */
    private array $letters;

    public function __construct(array $letters)
    {
        $this->letters = $letters;
    }

    protected function render(): string
    {
        $component = '';
        foreach ($this->letters as $letter) {
            $component .= ' '. $letter;
        }

        return '[' . $component . ' ]';
    }
}