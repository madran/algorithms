<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid\Expense;

use App\SOLID\OpenClosePrinciple\Valid\Expense;

class CarRentalExpense implements Expense
{
    private int $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getName(): string
    {
        return 'Car Rental';
    }

    public function isMeal(): bool
    {
        return false;
    }

    public function isExpensesExceeded(): bool
    {
        return false;
    }
}