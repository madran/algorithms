<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid\Expense;

use App\SOLID\OpenClosePrinciple\Valid\Expense;
use JetBrains\PhpStorm\Pure;

class BreakfastExpense implements Expense
{
    private int $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getName(): string
    {
        return 'Breakfast';
    }

    public function isMeal(): bool
    {
        return true;
    }

    #[Pure] public function isExpensesExceeded(): bool
    {
        return $this->getAmount() > 1000;
    }
}