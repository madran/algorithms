<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid\Expense;

use App\SOLID\OpenClosePrinciple\Valid\Expense;
use JetBrains\PhpStorm\Pure;

class DinnerExpense implements Expense
{
    private int $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getName(): string
    {
        return 'Dinner';
    }

    public function isMeal(): bool
    {
        return true;
    }

    #[Pure] public function isExpensesExceeded(): bool
    {
        return $this->getAmount() > 5000;
    }
}