<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid;

interface Expense
{
    public function getAmount(): int;
    public function getName(): string;
    public function isMeal(): bool;
    public function isExpensesExceeded(): bool;
}