<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid;

class ExpenseReport
{
    private ExpenseCalculator $expenseCalculator;

    private ReportPrinter $printer;

    public function __construct(ExpenseCalculator $expenseCalculator, ReportPrinter $printer)
    {
        $this->expenseCalculator = $expenseCalculator;
        $this->printer = $printer;
    }

    /**
     * @param Expense[] $expenses
     */
    public function print(array $expenses): void
    {
        $this->reportHeader();

        foreach ($expenses as $expense) {
            $this->printExpense($expense);
        }

        $this->expenseCalculator->calculate($expenses);

        $this->reportSummary($this->expenseCalculator->getMealExpenses(), $this->expenseCalculator->getTotal());
    }

    private function printExpense(Expense $expense): void
    {
        $this->printer->print(
            sprintf(
                "%s\t%s\t$%.02f\n",
                $expense->isExpensesExceeded() ? "X" : " ",
                $expense->getName(),
                $this->cents2Dollars($expense->getAmount())
            )
        );
    }

    public function reportHeader(): void
    {
        $this->printer->print('Expenses ' . $this->getDate() . PHP_EOL);
    }

    public function reportSummary(int $mealExpenses, int $total): void
    {
        $this->printer->print(sprintf("\nMeal expenses $%.02f", $this->cents2Dollars($mealExpenses)));
        $this->printer->print(sprintf("\nTotal $%.02f", $this->cents2Dollars($total)));
    }

    private function getDate(): string
    {
        return "9/12/2002";
    }

    private function cents2Dollars(int $mount): float
    {
        return $mount / 100.0;
    }
}