<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Valid;

class ExpenseCalculator
{
    private int $total = 0;

    private int $mealExpenses = 0;

    /**
     * @param Expense[] $expenses
     */
    public function calculate(array $expenses): void
    {
        foreach ($expenses as $expense) {
            $this->add($expense);
        }
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getMealExpenses(): int
    {
        return $this->mealExpenses;
    }

    protected function add(Expense $expense): void
    {
        if ($expense->isMeal()) {
            $this->mealExpenses += $expense->getAmount();
        }

        $this->total += $expense->getAmount();
    }
}