<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Invalid;

class ReportPrinter
{
    public function print(string $text): void
    {
        echo $text;
    }
}