<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Invalid;

class Expense {
    //Aby dodać nowy typ wydatku, należy dodać nową stałą.
    //Jeżeli z tej klasy korzysta wiele modułów (a wydaje się że to podstawowa klasa dla aplikacji) to będą wymagać rekompilacji.
    //Zmiana w tym miejscu może również spowodować nieprzewidywalne zachowania aplikacji w różnych miejscach.
    public const TYPE_DINNER = 'dinner';
    public const TYPE_BREAKFAST = 'breakfast';
    public const TYPE_CAR_RENTAL = 'car_rental';

    public const TYPES = [self::TYPE_BREAKFAST, self::TYPE_CAR_RENTAL, self::TYPE_DINNER];

    private string $type;
    private int $amount;

    public function __construct(string $type, int $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}