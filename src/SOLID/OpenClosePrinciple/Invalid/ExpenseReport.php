<?php
declare(strict_types=1);

namespace App\SOLID\OpenClosePrinciple\Invalid;

class ExpenseReport
{
    /** @var Expense[] */
    private array $expenses = [];

    public function printReport(ReportPrinter $printer): void
    {
        $total = 0;
        $mealExpenses = 0;

        $printer->print('Expenses ' . $this->getDate() . PHP_EOL);

        //Pętla wypisuje wszystkie koszta. Powinna być zamknięta w metodzie PrintExpenses().
        foreach ($this->expenses as $expense) {
            //Wydatki na jedzenie są sumowane dodatkowo osobno.
            $expenseType = $expense->getType();
            if ($expenseType === Expense::TYPE_BREAKFAST || $expenseType === Expense::TYPE_DINNER) {
                $mealExpenses += $expense->getAmount();
            }

            //Wyciągana jest nazwa zakupu.
            //Taki switch może pojawić się w wielu miejscach w systemie.
            $name = match ($expenseType) {
                Expense::TYPE_DINNER => "Dinner",
                Expense::TYPE_BREAKFAST => "Breakfast",
                Expense::TYPE_CAR_RENTAL => "Car Rental",
                default => "TILT",
            };

            //Wypisywany jest kolejny zakup gdzie obecność czy brak X jest uzależniony od tego czy wydatki nie przekroczyły zadanej kwoty.
            //Typy są na sztywno osadzone.
            $expenseAmount = $expense->getAmount();
            $printer->print(
                sprintf("%s\t%s\t$%.02f\n",
                    (($expenseType === Expense::TYPE_DINNER && $expenseAmount > 5000) || ($expenseType === Expense::TYPE_BREAKFAST && $expenseAmount > 1000)) ? "X" : " ",
                    $name, $expenseAmount / 100.0)
            );

            $total += $expenseAmount;
        }

        //W kilku miejscach zamieniane są pensy na dolary (dzielenie przez 100) to też powinno znaleźć się w osobnej metodzie
        $printer->print(sprintf("\nMeal expenses $%.02f", $mealExpenses / 100.0));
        $printer->print(sprintf("\nTotal $%.02f", $total / 100.0));
    }

    public function addExpense(Expense $expense): void
    {
        $this->expenses[] = $expense;
    }

    private function getDate(): string
    {
        return "9/12/2002";
    }
}